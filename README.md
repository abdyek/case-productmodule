# Installation

Clone repository
```sh
git clone https://abdyek@bitbucket.org/abdyek/case-productmodule.git product-module
```

Change directory
```sh
cd product-module
```

Install dependencies
```sh
composer install
```

Create a database on mysql(mariadb) and import *database.sql* file. If you need, change config in index.php

[Click](https://documenter.getpostman.com/view/11679387/UVR5qUUC) to show endpoints on Postman
