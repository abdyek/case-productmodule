<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require 'vendor/autoload.php';

use ProductModule\Core\Endpoint;
use ProductModule\Core\Config;
use ProductModule\Controller\ProductController;
use ProductModule\Controller\UserController;
use ProductModule\Controller\ExampleController;
use ProductModule\Model\ProductModel;
use ProductModule\Model\UserModel;
use Buki\Router\Router;

$config = new Config();

$config->setSecret('secret_key');

$config->setAuthorization([
    /*'ExampleController' => [
        'func1' => ['guest'],
        'func2' => ['member', 'guest']
    ],*/
    'UserController' => [
        'create'=>['guest'],
        'generateToken' => ['guest']
    ],
    'ProductController' => [
        'create' => ['member'],
        'update' => ['member'],
        'showAll' => ['member']
    ]
]);

$config->setDatabase([
    'dsn' => 'mysql:host=127.0.0.1:3306;dbname=shopside_case',
    'username' => 'root',
    'password' => ''
]);

$config->setRequiredMap([
    /*'ExampleController'=>[
        'func1'=> [
            'name'=>[
                'type'=>'str', // str, arr, int, email, bool, num
                'limits'=>[
                    'min'=>0,
                    'max'=>10
                ]
            ],
            'surname'=>[
                'type'=>'str', // str, arr, int
                'limits'=>[
                    'min'=>0,
                    'max'=>30
                ]
            ],
            'age'=>[
                'type'=>'num',
                'limits'=>[
                    'min'=>1,
                    'max'=>2
                ]
            ],
            'subArr' => [
                'number1'=>[
                    'type'=>'num',
                    'limits'=>[
                        'min'=>1,
                        'max'=>2
                    ]
                ],
                'number2'=>[
                    'type'=>'num',
                    'limits'=>[
                        'min'=>1,
                        'max'=>2
                    ]
                ],
            ]
        ]
    ],*/
    'UserController' => [
        'create' => [
            'email' => [
                'type' => 'str',
                'limits' => [
                    'min' => 1,
                    'max' => 255
                ]
            ],
            'password' => [
                'type' => 'str',
                'limits' => [
                    'min' => 6,
                    'max' => 50
                ]
            ]
        ],
        'generateToken' => [
            'email' => [
                'type' => 'str',
                'limits' => [
                    'min' => 1,
                    'max' => 255
                ]
            ],
            'password' => [
                'type' => 'str',
                'limits' => [
                    'min' => 6,
                    'max' => 50
                ]
            ]
        ]
    ],
    'ProductController' => [
        'create' => [
            'name' => [
                'type' => 'str',
                'limits' => [
                    'min' => 1,
                    'max' => 50
                ]
            ],
            'description' => [
                'type' => 'str',
                'limits' => [
                    'min' => 1,
                    'max' => 200
                ]
            ],
            'price' => [
                'type' => 'str',
                'limits' => [
                    'min' => 1,
                    'max' => 10
                ]
            ],
            'discount' => [
                'type' => 'bool',
            ],
            'salesStatus' => [
                'type' => 'str',
                'limits' => [
                    'min' => 1,
                    'max' => 50
                ]
            ]
        ],
        'update' => [
            'id'=>[
                'type' => 'int',
                'limits' => [
                    'min' => 1,
                    'max' => 11
                ]
            ],
            'name' => [
                'type' => 'str',
                'limits' => [
                    'min' => 1,
                    'max' => 50
                ]
            ],
            'description' => [
                'type' => 'str',
                'limits' => [
                    'min' => 1,
                    'max' => 200
                ]
            ],
            'price' => [
                'type' => 'str',
                'limits' => [
                    'min' => 1,
                    'max' => 10
                ]
            ],
            'discount' => [
                'type' => 'bool',
            ],
            'salesStatus' => [
                'type' => 'str',
                'limits' => [
                    'min' => 1,
                    'max' => 50
                ]
            ]
        ],
        'showAll' => [
            'page' => [
                'type' => 'num',
                'limits' => [
                    'min' => 1,
                    'max' => 10
                ]
            ]
        ]

    ]
]);

$router = new Router();

$router->post('/api/sign-up', function() {
    global $config;
    $endpoint = new Endpoint([new UserController(), 'create'], new UserModel(), $config);
    $endpoint->run();
    $endpoint->getResponse()->serve();
});

$router->post('/api/sign-in', function() {
    global $config;
    $endpoint = new Endpoint([new UserController(), 'generateToken'], new UserModel(), $config);
    $endpoint->run();
    $endpoint->getResponse()->serve();
});

$router->post('/api/product', function() {
    global $config;
    $endpoint = new Endpoint([new ProductController(), 'create'], new ProductModel(), $config);
    $endpoint->run();
    $endpoint->getResponse()->serve();
});

$router->put('/api/product', function() {
    global $config;
    $endpoint = new Endpoint([new ProductController(), 'update'], new ProductModel(), $config);
    $endpoint->run();
    $endpoint->getResponse()->serve();
});

$router->get('/api/products/:id', function($page) {
    global $config;
    $endpoint = new Endpoint([new ProductController(), 'showAll'], new ProductModel(), $config);
    $endpoint->getRequest()->addDataInUrl('page', $page);
    $endpoint->run();
    $endpoint->getResponse()->serve();
});

$router->run();
