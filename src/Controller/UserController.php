<?php

namespace ProductModule\Controller;

use ProductModule\Core\AbstractController;
use Firebase\JWT\JWT;

class UserController extends AbstractController
{
    // api/sign-up :: POST
    public function create(): void
    {
        $data = $this->request()->getData();
        $exist = $this->model()->getByEmail($data['email']);
        if($exist) {
            $response = $this->endpoint->getResponse();
            $response->conflict();
        }
        $this->model()->create([
            'email' => $data['email'],
            'password' => password_hash($data['password'], PASSWORD_DEFAULT)
        ]);
    }

    // api/sign-in :: POST
    public function generateToken(): void
    {
        $response = $this->endpoint->getResponse();
        $data = $this->request()->getData();
        $user = $this->model()->getByEmail($data['email']);

        if(!$user or !password_verify($data['password'], $user['password'])) {
            $response->unauthorized();
        }

        $jwt = JWT::encode([
            'iat' => time(),
            'exp' => time() + 300,
            'userId' => $user['id'],
            'who' => 'member',
        ], $this->endpoint->getConfig()->getSecret(), 'HS256');

        setCookie('jwt', $jwt, [
            'secure' => false,
            'path' => '/',
            'expires' => time() + 300,
            'httponly' => true,
            'samesite' => 'Lax'
        ]);
    }
}

