<?php

namespace ProductModule\Controller;

use ProductModule\Core\AbstractController;

class ProductController extends AbstractController
{
    // api/product :: POST
    public function create(): void
    {
        $this->model()->create(array_merge($this->request()->getData(), [
            'userId' => $this->endpoint->getAuthenticator()->getUserId()
        ]));
    }

    // api/product :: PUT
    public function update(): void
    {
        $data = $this->request()->getData();
        $model = $this->model();
        $product = $model->getById($data['id']);
        if(!$product) {
            $this->response()->notFound();
        }
        $model->update($data);
    }

    public function showAll(): void
    {
        $data = $this->request()->getData();
        $productCount = $this->model()->getAllCount();
        $page = (int)$data['page'];
        if($page < 1 or ($productCount <= ($page - 1) * 10)) {
            $this->response()->notFound();
        }
        $products = $this->model()->getAllByPageNumber($data['page']);
        $this->json([
            'products' => $products
        ]);
    }
}

