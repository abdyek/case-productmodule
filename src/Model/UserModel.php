<?php

namespace ProductModule\Model;

use ProductModule\Core\AbstractModel;

class UserModel extends AbstractModel 
{
    public function create(array $args): void
    {
        $this->endpoint->getDatabase()->execute('INSERT INTO user (email, password) VALUES(?,?)', [
            $args['email'],
            $args['password'],
        ]);
    }

    public function getByEmail($email): ?array
    {
        return $this->endpoint->getDatabase()->getRow('SELECT * FROM user WHERE email = ?', [$email]);
    }
}

