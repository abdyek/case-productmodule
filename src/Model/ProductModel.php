<?php

namespace ProductModule\Model;

use ProductModule\Core\AbstractModel;

class ProductModel extends AbstractModel
{
    public function create($args): void
    {
        $this->database()->execute('INSERT INTO product (name, description, price, discount, sales_status, user_id) VALUES(?,?,?,?,?,?)', [
            $args['name'],
            $args['description'],
            $args['price'],
            ($args['discount'] === true ? 1 : 0),
            $args['salesStatus'],
            $args['userId'],
        ]);
    }

    public function update($args): void
    {
        $this->database()->execute('UPDATE product SET name = ?, description = ?, price = ?, discount = ?, sales_status = ? WHERE id = ?', [
            $args['name'],
            $args['description'],
            $args['price'],
            ($args['discount'] === true ? 1: 0),
            $args['salesStatus'],
            $args['id'],
        ]);
    }

    public function getById($id): ?array
    {
        return $this->database()->getRow('SELECT * FROM product WHERE id = ?', [$id]);
    }

    public function getAllByPageNumber(int $page): ?array
    {
        $offset = (int)($page - 1) * 10;
        $rows = $this->database()->getRows('SELECT * FROM `product` LIMIT 10 OFFSET ' . $offset);
        foreach($rows as $key=>$row) {
            $rows[$key]['discount'] = ($row['discount'] == 1 ? true: false);
        }
        return $rows;
    }

    public function getAllCount(): int
    {
        return $this->database()->getRow('SELECT count(id) AS c FROM product')['c'];
    }
}
