<?php

namespace ProductModule\Core;

use ProductModule\Core\DefaultCore;
use ProductModule\Core\Database;

abstract class AbstractModel extends DefaultCore
{
    public function database(): Database
    {
        return $this->endpoint->getDatabase();
    }
    
}
