<?php

namespace ProductModule\Core;

use ProductModule\Core\DefaultCore;

class Response extends DefaultCore
{
    private int $responseCode;
    private array $responseData;

    public function __construct()
    {
        $this->responseCode = 200;
        $this->responseData = [];
    }

    public function serve(): void
    {
        header('Content-Type: application/json; charset=utf-8');
        http_response_code($this->responseCode);
        $responseArr = [
            'status' => 200,
            'message' => 'success',
            'content' => $this->responseData
        ];
        if(empty($this->responseData)) {
            unset($responseArr['content']);
        }
        if($this->responseCode === 200) {
            echo json_encode($responseArr);
        }
        die();
    }

    public function notFound(): void
    {
        $this->responseCode = 404;
        $this->serve();
    }

    public function forbidden(): void
    {
        $this->responseCode = 403;
        $this->serve();
    }

    public function unauthorized(): void
    {
        $this->responseCode = 401;
        $this->serve();
    }

    public function conflict(): void
    {
        $this->responseCode = 409;
        $this->serve();
    }

    public function badRequest(): void
    {
        $this->responseCode = 400;
        $this->serve();
    }

    public function getResponseCode(): int
    {
        return $this->responseCode;
    }

    public function setResponseCode(int $responseCode): void
    {
        $this->responseCode = $responseCode;
    }

    public function getResponseData(): array
    {
        return $this->responseData;
    }

    public function setResponseData(array $responseData): void
    {
        $this->responseData = $responseData;
    }
}
