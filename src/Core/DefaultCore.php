<?php

namespace ProductModule\Core;

use ProductModule\Core\Endpoint;

class DefaultCore
{
    protected Endpoint $endpoint;

    public function setEndpoint(Endpoint $endpoint)
    {
        $this->endpoint = $endpoint;
    }

}
