<?php

namespace ProductModule\Core;

use ProductModule\Core\DefaultCore;
use ProductModule\Core\AbstractModel;
use ProductModule\Core\Request;
use ProductModule\Core\Response;;

abstract class AbstractController extends DefaultCore
{
    public function get() {}
    public function post() {}
    public function put() {}
    public function patch() {}
    public function delete() {}

    public function getName(): string
    {
        $piece = explode('\\', get_class($this));
        return end($piece);
    }

    protected function json(array $data): void
    {
        $this->endpoint->getResponse()->setResponseData($data);
    }

    protected function response(): Response
    {
        return $this->endpoint->getResponse();
    }

    protected function request(): Request
    {
        return $this->endpoint->getRequest();
    }

    protected function model(): AbstractModel
    {
        return $this->endpoint->getModel();
    }
}
