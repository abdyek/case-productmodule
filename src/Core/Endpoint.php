<?php

namespace ProductModule\Core;

use ProductModule\Core\Request;
use ProductModule\Core\Validator;
use ProductModule\Core\Authenticator;
use ProductModule\Core\Database;
use ProductModule\Core\AbstractController;
use ProductModule\Core\AbstractModel;
use ProductModule\Core\Config;
use ProductModule\Core\Response;

class Endpoint
{
    private $request;
    private $validator;
    private $authenticator;
    private $database;
    private $controller;
    private $function;
    private $model;
    private $config;
    private $response;

    public function __construct(array $controller, AbstractModel $model = null, Config $config = null, Request $request = null, Validator $validator = null, Authenticator $authenticator = null, Database $database = null, Response $response = null)
    {
        $this->controller = $controller[0];
        $this->function = $controller[1];
        $this->model = $model;
        $this->request = $request ?? new Request();
        $this->validator = $validator ?? new Validator();
        $this->authenticator = $authenticator ?? new Authenticator();
        $this->database = $database ?? new Database();
        $this->config = $config ?? new Config();
        $this->response = $response ?? new Response();
    }
    public function run()
    {
        $this->prepareThis();
        if(!$this->authenticator->check()) {
            $this->response->forbidden();
        }
        $this->request->mergeData();
        if(!$this->validator->check()) {
            $this->response->badRequest();
        }
        $this->database->connect();
        $controller = $this->controller;
        $controller->{$this->function}();
    }

    private function prepareThis(): void
    {
        $this->request->setEndpoint($this);
        $this->validator->setEndpoint($this);
        $this->authenticator->setEndpoint($this);
        $this->database->setEndpoint($this);
        $this->controller->setEndpoint($this);
        $this->model->setEndpoint($this);
        $this->config->setEndpoint($this);
        $this->response->setEndpoint($this);
    }

    public function getRequest(): Request
    {
        return $this->request;
    }

    public function getAuthenticator(): Authenticator
    {
        return $this->authenticator;
    }
    
    public function getDatabase(): Database
    {
        return $this->database;
    }

    public function getController(): AbstractController
    {
        return $this->controller;
    }

    public function getFunction(): string
    {
        return $this->function;
    }

    public function getModel(): AbstractModel
    {
        return $this->model;
    }

    public function getConfig(): Config
    {
        return $this->config;
    }

    public function getResponse(): Response
    {
        return $this->response;
    }

}
