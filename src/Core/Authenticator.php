<?php

namespace ProductModule\Core;

use ProductModule\Core\DefaultCore;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class Authenticator extends DefaultCore
{
    private ?int $userId;
    private ?string $who = null;
    private ?array $payload;

    public function check(): bool
    {
        $this->solveJWT();
        $authorization = $this->endpoint->getConfig()->getAuthorization();
        $function = $this->endpoint->getFunction();
        $authorizationKeys = array_keys($authorization);
        $name = $this->endpoint->getController()->getName();
        if(!in_array($name, $authorizationKeys)) {
            return true;
        }
        $functions = array_keys($authorization[$name]);
        if(!in_array($function, $functions)) {
            return true;
        }
        if(in_array($this->who, $authorization[$name][$function])) {
            return true;
        }
        return false;
    }

    private function solveJWT(): void
    {
        $token = $this->endpoint->getRequest()->getToken();
        $secret = $this->endpoint->getConfig()->getSecret();
        try {
            $this->payload = (array) JWT::decode($token, new Key($secret, 'HS256'));
            $this->userId = $this->payload['userId'];
            $this->who = $this->payload['who'];
        } catch(\UnexpectedValueException $e) {
            $this->userId = null;
            $this->who = 'guest';
            $this->payload = null;
        }
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }
    
}
